<?php

use Illuminate\Database\Seeder;
use App\Mission;
class MissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Mission::create([
          'content' => 'Creating an organization strongly embraced by culture',
        ]);
        Mission::create([
          'content' => 'Providing relevant computer education to Binus students',
        ]);
        Mission::create([
          'content' => 'Building passionate communities of technology trends',
        ]);
        Mission::create([
          'content' => 'Developing reliable technology products and services',
        ]);
        Mission::create([
          'content' => 'Empower the organization\'s relation to the professional world',
        ]);
        Mission::create([
          'content' => 'Giving social contribution to the people of Indonesia',
        ]);
    }
}
