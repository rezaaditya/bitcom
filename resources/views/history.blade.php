<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link href="{{asset('assets/home/css/bootstrap.min.css')}}" rel="stylesheet">
	<link href="{{asset('assets/home/css/style.css')}}" rel="stylesheet">
	<link href="{{asset('assets/home/css/timeline.css')}}" rel="stylesheet">
	<link href="{{asset('assets/home/css/animate.css')}}" rel="stylesheet">
	<title>Document</title>
	<style media="screen">
		.container-fluid.history{
			background-color: black;
		}
		#animate1 {
			transition: transform 0.3s ease-out;
		}
				#animate2 {
			transition: transform 0.3s ease-out;
		}
		.zap {
			transform: scale(2.5, 0);
		}

		.history-content p{
			font-size: 14pt
		}
		.timeline{
			color: white;
		}
		.timeline>li{
			margin-bottom: 80px;
		}
		.timeline-panel, .timeline-badge{
			opacity: 0;
		}
		.item-show{
			opacity: 1;
		}
		.timeline:before {
				top:              0;
				bottom:           0;
				position:         absolute;
				content:          " ";
				width:            5px;
				background-color: #eeeeee;
				left:             50%;
				margin-left:      -2.5px;
		}
	</style>
</head>
<body>

	<div class="container-fluid history">
		<div class="row">
			<div class="col-lg-12">
				<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
					<h2 class="text-center">BNCC's History</h1>
					<div class="row">
						<div class="col-lg-12">
							<ul class="timeline">
								{{-- */ $i=0; /* --}}
								@foreach(App\History::all() as $row)
					        <li id="trigger{{$i}}" class="@if($i%2==1) {{"timeline-inverted"}} @endif">
					          <div class="timeline-badge">
					          	{{$row->year}}
					          </div>
					          <div class="timeline-panel">
					            <div id="animate1" class="timeline-heading">
					              {{-- <h4 class="timeline-title"></h4> --}}
					            </div>
					            <div class="timeline-body">
					              {!! $row->content !!}
					            </div>
					          </div>
					        </li>

										{{-- */ $i++; /* --}}
								@endforeach
    					</ul>
						</div>
					</div>

			<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
			<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
			<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
		</div>
		<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
		<script src="{{asset('assets/home/js/jquery-2.2.3.min.js')}}"></script>
		<!-- Include all compiled plugins (below), or include individual files as needed -->
		<script src="{{ asset('assets/home/js/bootstrap.min.js')}}"></script>
		<script src="{{ asset('assets/home/js/scrollmagic/uncompressed/ScrollMagic.js')}}"></script>
		{{-- <script src="{{ asset('assets/home/js/scrollmagic/uncompressed/plugins/debug.addIndicators.js')}}"></script> --}}
		<script src="{{ asset('assets/home/js/scrollmagic/uncompressed/plugins/jquery.ScrollMagic.js')}}"></script>
		<script type="text/javascript">
		// init controller
var controller = new ScrollMagic.Controller();
// build scene
	{{-- */ $i=0; /* --}}
	@foreach(App\History::all() as $row)
	new ScrollMagic.Scene({triggerElement: "#trigger{{$i}}"})
				.setClassToggle("#trigger{{$i}} .timeline-panel", "item-show animated fadeInLeft")
				.addIndicators({name: "{{$i}} - add a class"})
				.addTo(controller);
	new ScrollMagic.Scene({triggerElement: "#trigger{{$i}}"})
				.setClassToggle("#trigger{{$i}} .timeline-badge", "item-show animated bounceInUp")
				.addIndicators({name: "{{$i}} - add a class"})
				.addTo(controller);
														{{-- */ $i++; /* --}}
												@endforeach
		</script>
</body>
</html>
