<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Bina Nusantara Computer Club</title>

    <!-- Bootstrap -->
    <link href="{{asset('assets/home/css/bootstrap.min.css')}}" rel="stylesheet">
		<link href="{{asset('assets/home/css/animate.css')}}" rel="stylesheet">
  	<link href="{{asset('assets/home/css/style.css')}}" rel="stylesheet">
  	<link href="{{asset('accordion/accordion.css')}}" rel="stylesheet">
<style>
.contents{
	margin-top:100px;
}
body {
      background-image: url({{ asset('assets/img/bg.jpg') }});
      background-repeat: no-repeat;
      background-attachment: fixed;
      background-position: center;
      background-size: cover;
      background-color: rgba(0, 0, 0, 0.6);
      background-blend-mode: overlay;
    }
.pill{

	font-family: fashionism;
	font-size: 24pt;
	color: #0FB1DE;
}
.subdivision{

	font-family: fashionism;
	font-size: 18pt;
	color: #0FB1DE;
}
</style>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
		{{-- NAVBAR --}}
		@include('_navbar')
		{{-- END NAVBAR --}}
<div class="container contents">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="vision-mission text-center">OUR TEAM</h1>
			<ul class="nav nav-pills">
			    <li class="active"><a class="pill" data-toggle="pill" href="#bom">Board of Management</a></li>
			    <li><a class="pill" data-toggle="pill" href="#marketing">Marketing Division</a></li>
			    <li><a class="pill" data-toggle="pill" href="#product">Product Division</a></li>
			    <li><a class="pill" data-toggle="pill" href="#resource">Resource Division</a></li>
			    <li><a class="pill" data-toggle="pill" href="#technology">Technology Division</a></li>
			  </ul>
		</div>
		<div class="col-lg-12">
		
			
		</div>
		<div class="col-lg-12">
			
  
  <div class="tab-content">
    <div id="bom" class="tab-pane fade in active">
      <div class="pana-accordion" id="accordion">
		  <div class="pana-accordion-wrap">
		    <div class="pana-accordion-item"><img class="img-responsive" src="{{asset('assets/img/CEO.png')}}" /></div>
		    <div class="pana-accordion-item"><img class="img-responsive" src="{{asset('assets/img/CEO.png')}}" /></div>
		    <div class="pana-accordion-item"><img class="img-responsive" src="{{asset('assets/img/CEO.png')}}" /></div>
		    <div class="pana-accordion-item"><img class="img-responsive" src="{{asset('assets/img/CEO.png')}}" /></div>
		    <div class="pana-accordion-item"><img class="img-responsive" src="{{asset('assets/img/CEO.png')}}" /></div>
		    <div class="pana-accordion-item"><img class="img-responsive" src="{{asset('assets/img/CEO.png')}}" /></div>
		  </div>
		</div>
    </div>

    <div id="marketing" class="tab-pane fade">
      <h3 class="subdivision">External Event Organizer</h3>
      <div class="pana-accordion" id="accordion-1">
		  <div class="pana-accordion-wrap">
		    <div class="pana-accordion-item"><img class="img-responsive" src="{{asset('assets/img/CEO.png')}}" /></div>
		    <div class="pana-accordion-item"><img class="img-responsive" src="{{asset('assets/img/CEO.png')}}" /></div>
		    <div class="pana-accordion-item"><img class="img-responsive" src="{{asset('assets/img/CEO.png')}}" /></div>
		    <div class="pana-accordion-item"><img class="img-responsive" src="{{asset('assets/img/CEO.png')}}" /></div>
		    <div class="pana-accordion-item"><img class="img-responsive" src="{{asset('assets/img/CEO.png')}}" /></div>
		    <div class="pana-accordion-item"><img class="img-responsive" src="{{asset('assets/img/CEO.png')}}" /></div>
		  </div>
		</div>
      <h3 class="subdivision">Public Relation</h3>
      <div class="pana-accordion" id="accordion-2">
		  <div class="pana-accordion-wrap">
		    <div class="pana-accordion-item"><img class="img-responsive" src="{{asset('assets/img/CEO.png')}}" /></div>
		    <div class="pana-accordion-item"><img class="img-responsive" src="{{asset('assets/img/CEO.png')}}" /></div>
		    <div class="pana-accordion-item"><img class="img-responsive" src="{{asset('assets/img/CEO.png')}}" /></div>
		    <div class="pana-accordion-item"><img class="img-responsive" src="{{asset('assets/img/CEO.png')}}" /></div>
		    <div class="pana-accordion-item"><img class="img-responsive" src="{{asset('assets/img/CEO.png')}}" /></div>
		    <div class="pana-accordion-item"><img class="img-responsive" src="{{asset('assets/img/CEO.png')}}" /></div>
		  </div>
		</div>
    </div>

    <div id="product" class="tab-pane fade">
      
      <h3 class="subdivision">FAVE</h3>
      <div class="pana-accordion" id="accordion-3">
		  <div class="pana-accordion-wrap">
		    <div class="pana-accordion-item"><img class="img-responsive" src="{{asset('assets/img/CEO.png')}}" /></div>
		    <div class="pana-accordion-item"><img class="img-responsive" src="{{asset('assets/img/CEO.png')}}" /></div>
		    <div class="pana-accordion-item"><img class="img-responsive" src="{{asset('assets/img/CEO.png')}}" /></div>
		    <div class="pana-accordion-item"><img class="img-responsive" src="{{asset('assets/img/CEO.png')}}" /></div>
		    <div class="pana-accordion-item"><img class="img-responsive" src="{{asset('assets/img/CEO.png')}}" /></div>
		    <div class="pana-accordion-item"><img class="img-responsive" src="{{asset('assets/img/CEO.png')}}" /></div>
		  </div>
		</div>
      <h3 class="subdivision">Learning and Training</h3>
      <div class="pana-accordion" id="accordion-4">
		  <div class="pana-accordion-wrap">
		    <div class="pana-accordion-item"><img class="img-responsive" src="{{asset('assets/img/CEO.png')}}" /></div>
		    <div class="pana-accordion-item"><img class="img-responsive" src="{{asset('assets/img/CEO.png')}}" /></div>
		    <div class="pana-accordion-item"><img class="img-responsive" src="{{asset('assets/img/CEO.png')}}" /></div>
		    <div class="pana-accordion-item"><img class="img-responsive" src="{{asset('assets/img/CEO.png')}}" /></div>
		    <div class="pana-accordion-item"><img class="img-responsive" src="{{asset('assets/img/CEO.png')}}" /></div>
		    <div class="pana-accordion-item"><img class="img-responsive" src="{{asset('assets/img/CEO.png')}}" /></div>
		  </div>
		</div>
      <h3 class="subdivision">Magazine</h3>
      <div class="pana-accordion" id="accordion-5">
		  <div class="pana-accordion-wrap">
		    <div class="pana-accordion-item"><img class="img-responsive" src="{{asset('assets/img/CEO.png')}}" /></div>
		    <div class="pana-accordion-item"><img class="img-responsive" src="{{asset('assets/img/CEO.png')}}" /></div>
		    <div class="pana-accordion-item"><img class="img-responsive" src="{{asset('assets/img/CEO.png')}}" /></div>
		    <div class="pana-accordion-item"><img class="img-responsive" src="{{asset('assets/img/CEO.png')}}" /></div>
		    <div class="pana-accordion-item"><img class="img-responsive" src="{{asset('assets/img/CEO.png')}}" /></div>
		    <div class="pana-accordion-item"><img class="img-responsive" src="{{asset('assets/img/CEO.png')}}" /></div>
		  </div>
		</div>
    </div>

    <div id="resource" class="tab-pane fade">
      
      <h3 class="subdivision">Human Resource Development</h3>
      <div class="pana-accordion" id="accordion-6">
		  <div class="pana-accordion-wrap">
		    <div class="pana-accordion-item"><img class="img-responsive" src="{{asset('assets/img/CEO.png')}}" /></div>
		    <div class="pana-accordion-item"><img class="img-responsive" src="{{asset('assets/img/CEO.png')}}" /></div>
		    <div class="pana-accordion-item"><img class="img-responsive" src="{{asset('assets/img/CEO.png')}}" /></div>
		    <div class="pana-accordion-item"><img class="img-responsive" src="{{asset('assets/img/CEO.png')}}" /></div>
		    <div class="pana-accordion-item"><img class="img-responsive" src="{{asset('assets/img/CEO.png')}}" /></div>
		    <div class="pana-accordion-item"><img class="img-responsive" src="{{asset('assets/img/CEO.png')}}" /></div>
		  </div>
		</div>
      <h3 class="subdivision">Member Community</h3>
      <div class="pana-accordion" id="accordion-7">
		  <div class="pana-accordion-wrap">
		    <div class="pana-accordion-item"><img class="img-responsive" src="{{asset('assets/img/CEO.png')}}" /></div>
		    <div class="pana-accordion-item"><img class="img-responsive" src="{{asset('assets/img/CEO.png')}}" /></div>
		    <div class="pana-accordion-item"><img class="img-responsive" src="{{asset('assets/img/CEO.png')}}" /></div>
		    <div class="pana-accordion-item"><img class="img-responsive" src="{{asset('assets/img/CEO.png')}}" /></div>
		    <div class="pana-accordion-item"><img class="img-responsive" src="{{asset('assets/img/CEO.png')}}" /></div>
		    <div class="pana-accordion-item"><img class="img-responsive" src="{{asset('assets/img/CEO.png')}}" /></div>
		  </div>
		</div>
    </div>

    <div id="technology" class="tab-pane fade">
      
      <h3 class="subdivision">IT Resource Development</h3>
      <div class="pana-accordion" id="accordion-8">
		  <div class="pana-accordion-wrap">
		    <div class="pana-accordion-item"><img class="img-responsive" src="{{asset('assets/img/CEO.png')}}" /></div>
		    <div class="pana-accordion-item"><img class="img-responsive" src="{{asset('assets/img/CEO.png')}}" /></div>
		    <div class="pana-accordion-item"><img class="img-responsive" src="{{asset('assets/img/CEO.png')}}" /></div>
		    <div class="pana-accordion-item"><img class="img-responsive" src="{{asset('assets/img/CEO.png')}}" /></div>
		    <div class="pana-accordion-item"><img class="img-responsive" src="{{asset('assets/img/CEO.png')}}" /></div>
		    <div class="pana-accordion-item"><img class="img-responsive" src="{{asset('assets/img/CEO.png')}}" /></div>
		  </div>
		</div>
      <h3 class="subdivision">Research and Development</h3>
      <div class="pana-accordion" id="accordion-9">
		  <div class="pana-accordion-wrap">
		    <div class="pana-accordion-item"><img class="img-responsive" src="{{asset('assets/img/CEO.png')}}" /></div>
		    <div class="pana-accordion-item"><img class="img-responsive" src="{{asset('assets/img/CEO.png')}}" /></div>
		    <div class="pana-accordion-item"><img class="img-responsive" src="{{asset('assets/img/CEO.png')}}" /></div>
		    <div class="pana-accordion-item"><img class="img-responsive" src="{{asset('assets/img/CEO.png')}}" /></div>
		    <div class="pana-accordion-item"><img class="img-responsive" src="{{asset('assets/img/CEO.png')}}" /></div>
		    <div class="pana-accordion-item"><img class="img-responsive" src="{{asset('assets/img/CEO.png')}}" /></div>
		  </div>
		</div>
    </div>

  </div>
		</div>
	</div>
</div>


    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="{{asset('assets/home/js/jquery-2.2.3.min.js')}}"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="{{ asset('assets/home/js/bootstrap.min.js')}}"></script>
    <script src="{{ asset('assets/home/js/flickity.pkgd.min.js')}}"></script>
    <script src="{{ asset('assets/home/js/scrollmagic/uncompressed/ScrollMagic.js')}}"></script>
		<!-- <script src="{{ asset('assets/home/js/scrollmagic/uncompressed/plugins/debug.addIndicators.js')}}"></script> -->
		<!-- // <script src="{{ asset('assets/home/js/scrollmagic/uncompressed/plugins/jquery.ScrollMagic.js')}}"></script> -->
		<script src="{{ asset('assets/home/js/navbar.js')}}"></script>
  <script type="text/javascript" src="{{asset('accordion/pana-accordion.js')}}"></script>
    <script>
      $('.main-carousel').flickity({
        cellAlign:'left',
        contain:true,
        wrapAround: true
      });

      $(function(){
	    accordion.init({
	      id: 'accordion'
	    });
	  });
      $(function(){
	    accordion.init({
	      id: 'accordion-1'
	    });
	  });
      $(function(){
	    accordion.init({
	      id: 'accordion-2'
	    });
	  });
      $(function(){
	    accordion.init({
	      id: 'accordion-3'
	    });
	  });
      $(function(){
	    accordion.init({
	      id: 'accordion-4'
	    });
	  });
      $(function(){
	    accordion.init({
	      id: 'accordion-5'
	    });
	  });
      $(function(){
	    accordion.init({
	      id: 'accordion-6'
	    });
	  });
      $(function(){
	    accordion.init({
	      id: 'accordion-7'
	    });
	  });
      $(function(){
	    accordion.init({
	      id: 'accordion-8'
	    });
	  });
      $(function(){
	    accordion.init({
	      id: 'accordion-9'
	    });
	  });

    </script>
  </body>
</html>
