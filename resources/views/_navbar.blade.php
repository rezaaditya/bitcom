<nav id="header" class="navbar navbar-fixed-top">
    <div id="header-container" class="container navbar-container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="glyphicon glyphicon-menu-hamburger"></span>
            </button>
            <a id="brand" class="navbar-brand" href="{{ url('') }}">
            <img class="logo" src="{{ asset('assets/img/BNCC_T.png') }}"></img>
            </a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li><a href="{{ url('') }}">About</a></li>
                <li><a href="{{ url('news') }}">News</a></li>
                <li><a href="{{ url('event') }}">Event</a></li>
                <li><a href="{{ url('product') }}">Product</a></li>
                <li><a href="{{ url('team') }}">Our Team</a></li>
                <!-- <li><a href="{{ url('') }}">Contact</a></li> -->
                <!-- <li><a href="{{ url('alumnus') }}">Alumnus</a></li> -->
            </ul>
        </div><!-- /.nav-collapse -->
    </div><!-- /.container -->
</nav><!-- /.navbar -->