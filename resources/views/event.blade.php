<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Bina Nusantara Computer Club</title>

    <!-- Bootstrap -->
    <link href="{{asset('assets/home/css/bootstrap.min.css')}}" rel="stylesheet">
		<link href="{{asset('assets/home/css/animate.css')}}" rel="stylesheet">
  	<link href="{{asset('assets/home/css/style.css')}}" rel="stylesheet">
  	<link href="{{asset('accordion/accordion.css')}}" rel="stylesheet">
	  <link rel="stylesheet" href="{{asset('assets/home/css/flickity.css')}}">
<style>
.contents{
	margin-top:100px;
}
body {
      background-image: url({{ asset('assets/img/bg.jpg') }});
      background-repeat: no-repeat;
      background-attachment: fixed;
      background-position: center;
      background-size: cover;
      background-color: rgba(0, 0, 0, 0.6);
      background-blend-mode: overlay;
    }
.pill{

	font-family: fashionism;
	font-size: 24pt;
	color: #0FB1DE;
}
.subdivision{

	font-family: fashionism;
	font-size: 18pt;
	color: #0FB1DE;
}


.main-carousel{
	width:100%;
	height:200px;
}

.carousel-cell {
  width: 50%; /* half-width */
  height: 200px;
  margin-right: 10px;
  color:white;
  background-image: url({{ asset('images/picture_1.jpg') }});
      background-repeat: no-repeat;
      background-position: center;
      background-size: cover;
}

.flickity-page-dots{
  bottom:0px;
}

</style>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
		{{-- NAVBAR --}}
		@include('_navbar')
		{{-- END NAVBAR --}}
<div class="container contents">
	<h1 class="vision-mission text-center">OUR EVENTS</h1>

	<p class="subdivision">May 2016</p>
	<div class="row">
		<div class="col-lg-12">
			<div class="main-carousel">
		    @for($i=0;$i<5;$i++)
		    <div class="carousel-cell">
				<div class="row">
					<div class="col-lg-12">
						<p class="text-center vision-mission">Event One</p>
						<p class="vision-description text-center">
							Lorem ipsum dolor sit amet.
						</p>
					</div>

				</div>
			</div>
		    @endfor
  			</div>
		</div>
	</div>

	<p class="subdivision">April 2016</p>
	<div class="row">
		<div class="col-lg-12">
			<div class="main-carousel">
		    @for($i=0;$i<5;$i++)
		    <div class="carousel-cell">
				<div class="row">
					<div class="col-lg-12">
						<p class="text-center vision-mission">Event One</p>
						<p class="vision-description text-center">
							Lorem ipsum dolor sit amet.
						</p>
					</div>

				</div>
			</div>
		    @endfor
  			</div>
		</div>
	</div>

	<p class="subdivision">March 2016</p>
	<div class="row">
		<div class="col-lg-12">
			<div class="main-carousel">
		    @for($i=0;$i<5;$i++)
		    <div class="carousel-cell">
				<div class="row">
					<div class="col-lg-12">
						<p class="text-center vision-mission">Event One</p>
						<p class="vision-description text-center">
							Lorem ipsum dolor sit amet.
						</p>
					</div>

				</div>
			</div>
		    @endfor
  			</div>
		</div>
	</div>

</div>


    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="{{asset('assets/home/js/jquery-2.2.3.min.js')}}"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="{{ asset('assets/home/js/bootstrap.min.js')}}"></script>
    <script src="{{ asset('assets/home/js/flickity.pkgd.min.js')}}"></script>
    <script src="{{ asset('assets/home/js/scrollmagic/uncompressed/ScrollMagic.js')}}"></script>
		<!-- <script src="{{ asset('assets/home/js/scrollmagic/uncompressed/plugins/debug.addIndicators.js')}}"></script> -->
		<!-- // <script src="{{ asset('assets/home/js/scrollmagic/uncompressed/plugins/jquery.ScrollMagic.js')}}"></script> -->
		<script src="{{ asset('assets/home/js/navbar.js')}}"></script>
  <script type="text/javascript" src="{{asset('accordion/pana-accordion.js')}}"></script>
    <script>
      $('.main-carousel').flickity({
        cellAlign:'left',
        contain:true,
        wrapAround: true
      });

      $(function(){
	    accordion.init({
	      id: 'accordion'
	    });
	  });
      $(function(){
	    accordion.init({
	      id: 'accordion-1'
	    });
	  });
      $(function(){
	    accordion.init({
	      id: 'accordion-2'
	    });
	  });
      $(function(){
	    accordion.init({
	      id: 'accordion-3'
	    });
	  });
      $(function(){
	    accordion.init({
	      id: 'accordion-4'
	    });
	  });
      $(function(){
	    accordion.init({
	      id: 'accordion-5'
	    });
	  });
      $(function(){
	    accordion.init({
	      id: 'accordion-6'
	    });
	  });
      $(function(){
	    accordion.init({
	      id: 'accordion-7'
	    });
	  });
      $(function(){
	    accordion.init({
	      id: 'accordion-8'
	    });
	  });
      $(function(){
	    accordion.init({
	      id: 'accordion-9'
	    });
	  });

    </script>
  </body>
</html>
