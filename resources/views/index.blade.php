<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Bina Nusantara Computer Club</title>

    <!-- Bootstrap -->
    <link href="{{asset('assets/home/css/bootstrap.min.css')}}" rel="stylesheet">
  	<link href="{{asset('assets/home/css/timeline.css')}}" rel="stylesheet">
		<link href="{{asset('assets/home/css/animate.css')}}" rel="stylesheet">
	  <link rel="stylesheet" href="{{asset('assets/home/css/flickity.css')}}">
  	<link href="{{asset('assets/home/css/style.css')}}" rel="stylesheet">
<style>

.home-welcome{
	height:100vh;
	width:100%;
	color:white;
}

.home-welcome .container{
	padding-top: 46vh;
}
.main-carousel{
	width:100%;
	height:100vh;
  background-image:url("assets/img/LDP 27.jpg");
  background: cover no-repeat center fixed;
  background-size: cover;
	background-color: rgba(0, 0, 0, 0.6);
	background-blend-mode: overlay;
  /*background-color: gray;*/
}
.carousel-cell{
	width: 100%;
	height: 100%;
	padding: 60px 80px;


	color:white;
}


.vision-description{
	font-size:24pt;
}

.mission-items{
	margin-bottom: 40px;
}

.mission-img{
  width:40px;
  height: 40px;
}

.flickity-page-dots{
  bottom:20px;
}

#background {
    position: fixed;
    top: 50%;
    left: 50%;
    min-width: 100%;
    min-height: 100%;
    width: auto;
    height: auto;
    z-index: -100;
    -webkit-transform: translateX(-50%) translateY(-50%);
    transform: translateX(-50%) translateY(-50%);
    background-size: cover;
}


.contact label{
	color:white;
}


/*HISTORY*/
.container-fluid.history{
			/*background-color: black;*/
			background-image: url({{ asset('assets/img/bg.jpg') }});
			background-repeat: no-repeat;
			background-attachment: fixed;
			background-position: center;
  			background-size: cover;
			background-color: rgba(0, 0, 0, 0.6);
			background-blend-mode: overlay;
		}
		#animate1 {
			transition: transform 0.3s ease-out;
		}
				#animate2 {
			transition: transform 0.3s ease-out;
		}
		.zap {
			transform: scale(2.5, 0);
		}

		.history-content p{
			font-size: 14pt
		}
		.timeline{
			color: white;
		}
		.timeline>li{
			margin-bottom: 80px;
		}
		.timeline-panel, .timeline-badge{
			opacity: 0;
		}
		.item-show{
			opacity: 1;
		}
		.timeline:before {
				top:              0;
				bottom:           0;
				position:         absolute;
				content:          " ";
				width:            5px;
				background-color: #eeeeee;
				left:             50%;
				margin-left:      -2.5px;
		}
/*END HISTORY*/
</style>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
		{{-- NAVBAR --}}
		@include('_navbar')
		{{-- END NAVBAR --}}
		<video autoplay loop muted poster="screenshot.jpg" id="background">
        <source src="{{ asset('vid/video.mp4') }}" type="video/mp4">
    </video>
		<div class="home-welcome">
			<div class="container">
				<div class="col-lg-6 col-lg-offset-3">
					<img src="{{asset('assets/img/BNCC_T.png')}}" class="img-responsive" alt="Placeholder">
				</div>
				<!-- <h1 class="text-center">BNCC</h1>
				<p class="text-center">Bina Nusantara Computer Club</p> -->
			</div>
		</div>
    <div class="main-carousel">



			<!-- About BNCC -->
	    <div class="carousel-cell">
				<div class="row">
					<div class="col-lg-12">
						<p class="text-center vision-mission">WHAT IS BNCC?</p>
					</div>
					<div class="col-lg-6">

							<img src="{{asset('assets/img/BNCC_T.png')}}" class="img-responsive" alt="Placeholder">

					</div>
					<div class="col-lg-6">
						<br>
						<p><b>Bina Nusantara Computer Club</b> is a computer-based organization in Binus University and we are a family of excellent youth who works passionately and professionally.
						<p>Established on 1989, BNCC has now reached its 26th year. It has been a long milestone for us to create a strong foundation and belief to learn and forge ourselves with computer, technology business, and organizational skills.</p>
						<p><b>BNCC</b> educate others in computer and organizational knowledge, having technology products such as software house and online magazine, doing technology research, and creating technology communities inside and outside the university.</p>
						<p>Be successful by joining us. Be a part of our family and relation and become one of the future generation, a generation which will always give innovations on the technology era.</p></p>
					</div>
				</div>
			</div>


			{{-- VISION --}}
	    <div class="carousel-cell">
				<div class="row">
					<div class="col-lg-12">
						<p class="text-center vision-mission">OUR VISION</p>
						<p class="vision-description text-center">
							An organization of excellent youth in continuous development of technology innovation, products, and services for the people.
						</p>
					</div>

				</div>
			</div>



			{{-- MISSION --}}
	    <div class="carousel-cell">
				<div class="row">
					<div class="col-lg-12">
						<p class="text-center vision-mission">OUR MISSION</p>
					</div>

            <br><br>
						@for($i=1; $i < 7 ; $i++)
							<div class="col-md-6 col-sm-12">
							@if ($i%2==1)
								<div class="row mission-items">
									<div class="col-sm-10 col-xs-12">
										<p class="text-right">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Excepturi, obcaecati.</p>
									</div>
									<div class="col-sm-2 hidden-xs"><img class="mission-img img-responsive" src="{{ asset('assets/img/mission-'.($i).'.png') }}" alt="Placeholder"></div>
								</div>
								@else
								<div class="row mission-items">
	                <div class="col-sm-2 hidden-xs"><img class="mission-img img-responsive" src="{{ asset('assets/img/mission-'.($i).'.png') }}" alt="Placeholder"></div>
	                <div class="col-sm-10 col-xs-12">
	                  <p class="text-left">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Excepturi, obcaecati.</p>
	                </div>
	              </div>
							@endif
							</div>
            @endfor


				</div>
			</div>
  </div>


  <!-- HISTORY TIMELINE -->
		<div class="container-fluid history">
		<div class="row">
			<div class="col-lg-12">
				<p class="text-center vision-mission">HISTORY</p>
				<div class="row">
					<div class="col-lg-12">
						<ul class="timeline">
							{{-- */ $i=0; /* --}}
							@foreach(App\History::all() as $row)
					        <li id="trigger{{$i}}" class="@if($i%2==1) {{"timeline-inverted"}} @endif">
					          <div class="timeline-badge">
					          	{{$row->year}}
					          </div>
					          <div class="timeline-panel">
					            <div id="animate1" class="timeline-heading">
					              {{-- <h4 class="timeline-title"></h4> --}}
					            </div>
					            <div class="timeline-body">
					              {!! $row->content !!}
					            </div>
					          </div>
					        </li>

										{{-- */ $i++; /* --}}
								@endforeach
    					</ul>
					</div>
				</div>

		</div>
	</div>
	</div>

	<div class="container-fluid contact">
		<div class="row">
			<div class="col-md-12">
				<p class="vision-mission text-center">CONTACT US</p>
			</div>
			<div class="col-md-6 col-md-offset-3">
				<form>
				  <div class="form-group">
				    <label for="exampleInputEmail1">Name</label>
				    <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Name">
				  </div>
				  <div class="form-group">
				    <label for="exampleInputPassword1">Email</label>
				    <input type="text" class="form-control" id="exampleInputPassword1" placeholder="Email">
				  </div>
				  <div class="form-group">
				    <label for="exampleInputEmail1">Message</label>
				    <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Message">
				  </div>
  					<button type="submit" class="btn btn-default">Submit</button>
				  </form>
			</div>
		</div>
	</div>
		<!-- ENDHISTORY TIMELINE -->
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="{{asset('assets/home/js/jquery-2.2.3.min.js')}}"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="{{ asset('assets/home/js/bootstrap.min.js')}}"></script>
    <script src="{{ asset('assets/home/js/flickity.pkgd.min.js')}}"></script>
    <script src="{{ asset('assets/home/js/scrollmagic/uncompressed/ScrollMagic.js')}}"></script>
		<!-- <script src="{{ asset('assets/home/js/scrollmagic/uncompressed/plugins/debug.addIndicators.js')}}"></script> -->
		<script src="{{ asset('assets/home/js/scrollmagic/uncompressed/plugins/jquery.ScrollMagic.js')}}"></script>
		<script src="{{ asset('assets/home/js/navbar.js')}}"></script>
    <script>
      $('.main-carousel').flickity({
        cellAlign:'left',
        contain:true,
        wrapAround: true
      });

      $(document).ready(function(){





// HISTORY

// init controller
var controller = new ScrollMagic.Controller();
// build scene
	{{-- */ $i=0; /* --}}
	@foreach(App\History::all() as $row)
	new ScrollMagic.Scene({triggerElement: "#trigger{{$i}}"})
				.setClassToggle("#trigger{{$i}} .timeline-panel", "item-show animated fadeInLeft")
				// .addIndicators({name: "{{$i}} - add a class"})
				.addTo(controller);
	new ScrollMagic.Scene({triggerElement: "#trigger{{$i}}"})
				.setClassToggle("#trigger{{$i}} .timeline-badge", "item-show animated bounceInUp")
				// .addIndicators({name: "{{$i}} - add a class"})
				.addTo(controller);
														{{-- */ $i++; /* --}}
												@endforeach
//
});
    </script>
  </body>
</html>
