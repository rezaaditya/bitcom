<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <title>jQuery Honeycombs Plugin Demo</title>
  <link rel="stylesheet" href="css/normalize.min.css">
  <link rel="stylesheet" href="css/animate.min.css">
  <link rel="stylesheet" type="text/css" href="homeycombs/css/homeycombs.css" />
    <link href="{{asset('assets/home/css/animate.css')}}" rel="stylesheet">
  <link href="{{asset('assets/home/css/bootstrap.min.css')}}" rel="stylesheet">
  <link href="{{asset('assets/home/css/style.css')}}" rel="stylesheet">
  <style>
    body {
      background-image: url({{ asset('assets/img/bg.jpg') }});
      background-repeat: no-repeat;
      background-attachment: fixed;
      background-position: center;
      background-size: cover;
      background-color: rgba(0, 0, 0, 0.6);
      background-blend-mode: overlay;
    }

    .container-fluid {
      padding-top: 100px;
    }

    .modal-desc{
      padding: 20px;
    }
    .btn-circle {
      width: 30px;
      height: 30px;
      text-align: center;
      padding: 6px 0;
      font-size: 12px;
      line-height: 1.42;
      border-radius: 15px;
    }
    .header {
      font-family: fashionism;
      font-size: 60pt;
      color: #0FB1DE;
    }
  </style>
</head>
<body>
    {{-- NAVBAR --}}
    @include('_navbar')
    {{-- END NAVBAR --}}
  <div class="container-fluid">
  <p class="text-center header">NEWS</p>

    <div class="row">
     <div class="col-lg-10 col-lg-offset-1">
      <div class="honeycombs">
       @for($i=20; $i > 5; $i--)
       <a id="demo01" href="#animatedModal">
         <div class="comb animate bounceInUp"> <img src="images/picture_1.jpg" />

           <span>
            <b>AOCT WINNER</b>
            <br>
            {{$i}} April 2016
          </span>

        </div>
      </a>
      @endfor
    </div>
  </div>
</div>
</div>

<div id="animatedModal">
  <!--THIS IS IMPORTANT! to close the modal, the class name has to match the name given on the ID -->
  <div id="btn-close-modal" class="close-animatedModal text-center">
    <br>
    <a href="#" class="btn btn-default btn-circle">
      <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
    </a>

  </div>

  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <h2 class="text-center">AOCT Winner
        <br><small>31 April 2016</small></h2>
      </div>
      <div class="col-lg-6"><img class="img-responsive" src="http://placehold.it/600x400"></div>
      <div class="col-lg-6" class="modal-desc">
        <p>
          Lorem ipsum dolor sit amet, consectetur adipisicing elit. Suscipit repellat laboriosam molestiae aut, nam odit consequatur mollitia minima nesciunt dolore quibusdam, culpa excepturi ab itaque modi nobis, iusto asperiores repellendus.
        </p>
        <p>
          Lorem ipsum dolor sit amet, consectetur adipisicing elit. Suscipit repellat laboriosam molestiae aut, nam odit consequatur mollitia minima nesciunt dolore quibusdam, culpa excepturi ab itaque modi nobis, iusto asperiores repellendus.
        </p>
        <p>
          Lorem ipsum dolor sit amet, consectetur adipisicing elit. Suscipit repellat laboriosam molestiae aut, nam odit consequatur mollitia minima nesciunt dolore quibusdam, culpa excepturi ab itaque modi nobis, iusto asperiores repellendus.
        </p>
        <p>
          Lorem ipsum dolor sit amet, consectetur adipisicing elit. Suscipit repellat laboriosam molestiae aut, nam odit consequatur mollitia minima nesciunt dolore quibusdam, culpa excepturi ab itaque modi nobis, iusto asperiores repellendus.
        </p>
        <p>
          Lorem ipsum dolor sit amet, consectetur adipisicing elit. Suscipit repellat laboriosam molestiae aut, nam odit consequatur mollitia minima nesciunt dolore quibusdam, culpa excepturi ab itaque modi nobis, iusto asperiores repellendus.
        </p>
        <p>
          Lorem ipsum dolor sit amet, consectetur adipisicing elit. Suscipit repellat laboriosam molestiae aut, nam odit consequatur mollitia minima nesciunt dolore quibusdam, culpa excepturi ab itaque modi nobis, iusto asperiores repellendus.
        </p>
        <p>
          Lorem ipsum dolor sit amet, consectetur adipisicing elit. Suscipit repellat laboriosam molestiae aut, nam odit consequatur mollitia minima nesciunt dolore quibusdam, culpa excepturi ab itaque modi nobis, iusto asperiores repellendus.
        </p>
        <p>
          Lorem ipsum dolor sit amet, consectetur adipisicing elit. Suscipit repellat laboriosam molestiae aut, nam odit consequatur mollitia minima nesciunt dolore quibusdam, culpa excepturi ab itaque modi nobis, iusto asperiores repellendus.
        </p>
        <p>
          Lorem ipsum dolor sit amet, consectetur adipisicing elit. Suscipit repellat laboriosam molestiae aut, nam odit consequatur mollitia minima nesciunt dolore quibusdam, culpa excepturi ab itaque modi nobis, iusto asperiores repellendus.
        </p>
        <p>
          Lorem ipsum dolor sit amet, consectetur adipisicing elit. Suscipit repellat laboriosam molestiae aut, nam odit consequatur mollitia minima nesciunt dolore quibusdam, culpa excepturi ab itaque modi nobis, iusto asperiores repellendus.
        </p>
        <p>
          Lorem ipsum dolor sit amet, consectetur adipisicing elit. Suscipit repellat laboriosam molestiae aut, nam odit consequatur mollitia minima nesciunt dolore quibusdam, culpa excepturi ab itaque modi nobis, iusto asperiores repellendus.
        </p>
      </div>
    </div>
  </div>

</div>


<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script src="homeycombs/js/jquery.homeycombs.js"></script>
<script src="js/animatedModal.min.js"></script>
<script src="{{ asset('assets/home/js/navbar.js')}}"></script>
<script>


          //demo 02
          $("#demo01").animatedModal({
            animatedIn:'bounceIn',
            animatedOut:'bounceOutDown',
            animationDuration:'.8s',
            color:'#fff',
              // Callbacks
              beforeOpen: function() {
                console.log("The animation was called");
              },
              afterOpen: function() {
                console.log("The animation is completed");
              },
              beforeClose: function() {
                console.log("The animation was called");
              },
              afterClose: function() {
                console.log("The animation is completed");
              }
            });

          </script>
          <script type="text/javascript">
            $(document).ready(function() {
              $('.honeycombs').honeycombs();
            });
          </script>

        </body>
        </html>
