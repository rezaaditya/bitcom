<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Bina Nusantara Computer Club</title>

    <!-- Bootstrap -->
    <link href="{{asset('assets/home/css/bootstrap.min.css')}}" rel="stylesheet">
  	<link href="{{asset('assets/home/css/timeline.css')}}" rel="stylesheet">
		<link href="{{asset('assets/home/css/animate.css')}}" rel="stylesheet">
	  <link rel="stylesheet" href="{{asset('assets/home/css/flickity.css')}}">
  	<link href="{{asset('assets/home/css/style.css')}}" rel="stylesheet">
<style>

body {
      background-image: url({{ asset('assets/img/history-bg.jpg') }});
      background: cover no-repeat center fixed;
      background-size: cover;
      background-color: rgba(0, 0, 0, 0.6);
      background-blend-mode: overlay;
    }

.home-welcome{
	height:100vh;
	width:100%;
	color:white;
}

.home-welcome .container{
	padding-top: 46vh;
}
.main-carousel{
	width:100%;
	height:600px;
  /*background-image:url("assets/img/LDP 27.jpg");
  background: cover no-repeat center fixed;
  background-size: cover;
	background-color: rgba(0, 0, 0, 0.6);
	background-blend-mode: overlay;*/
  background-color: gray;
}
.carousel-cell{
	width: 100%;
	height: 100%;
	padding: 60px 80px;


	color:white;
}

.c1{
	background-color: pink;
}
.c2{
	background-color: teal;
}
.c2{
	background-color: blue;
}

.product-text{
	font-family: fashionism;
	font-size: 20pt;
	color: white;
}

.vision-mission{
	font-family: fashionism;
	font-size: 60pt;
	color: #0FB1DE;
}
.vision-description{
	font-size:24pt;
}

.mission-items{
	margin-bottom: 40px;
}

.mission-img{
  width:40px;
  height: 40px;
}

.flickity-page-dots{
  bottom:20px;
}

#background {
    position: fixed;
    top: 50%;
    left: 50%;
    min-width: 100%;
    min-height: 100%;
    width: auto;
    height: auto;
    z-index: -100;
    -webkit-transform: translateX(-50%) translateY(-50%);
    transform: translateX(-50%) translateY(-50%);
    background: url(polina.jpg) no-repeat;
    background-size: cover;
}

.content{
	margin-top: 100px;
}

</style>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
		{{-- NAVBAR --}}
		@include('_navbar')
		{{-- END NAVBAR --}}

    <div class="container content">
       <p class="text-center vision-mission">Product</p>
    	<div class="row">
    	<br>
	    	<div class="col-lg-4">
	    		<a href="#"><img class="img-responsive" src="assets/img/fave.png"></a>
	    	</div>
	    	<div class="col-lg-8">
	    		<p class="product-text">A professional project house which focused on development and service<br>
						Lorem ipsum dolor sit amet, consectetur adipisicing elit.
					</p>
	    	</div>
    	</div>
    	<div class="row">
    		<div class="col-lg-4">
	    		<a href="#"><img class="img-responsive" src="assets/img/file.png"></a>
	    	</div>
	    	<div class="col-lg-8">
	    		<p class="product-text">Free online e-Magazine providing IT news feed, games, tips and many more.
						Lorem ipsum dolor sit amet, consectetur adipisicing elit.
	    		</p>
	    	</div>

    	</div>
    	<div class="row">

	    	<div class="col-lg-4">
	    		<a href="#"><img class="img-responsive" src="assets/img/lnt.png"></a>
    		</div>
	    	<div class="col-lg-8">
	    		<p class="product-text">LnT (Learning and Training) provides one-year period of courses based on computer for BINUS University Students.
						Lorem ipsum dolor sit amet, consectetur adipisicing elit.
    	</div>
    </div>


    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="{{asset('assets/home/js/jquery-2.2.3.min.js')}}"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="{{ asset('assets/home/js/bootstrap.min.js')}}"></script>
    <script src="{{ asset('assets/home/js/flickity.pkgd.min.js')}}"></script>
    <script src="{{ asset('assets/home/js/scrollmagic/uncompressed/ScrollMagic.js')}}"></script>
		<!-- <script src="{{ asset('assets/home/js/scrollmagic/uncompressed/plugins/debug.addIndicators.js')}}"></script> -->
		<script src="{{ asset('assets/home/js/scrollmagic/uncompressed/plugins/jquery.ScrollMagic.js')}}"></script>
		<script src="{{ asset('assets/home/js/navbar.js')}}"></script>
    <script>
      $('.main-carousel').flickity({
        cellAlign:'left',
        contain:true,
        wrapAround: true
      });

    </script>
  </body>
</html>
